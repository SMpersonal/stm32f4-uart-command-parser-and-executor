#ifndef __DAC_H
#define __DAC_H
#include "stm32f4xx.h"
#include "delay.h"
#include "usart.h"

void initDAC1(void);	
void initDAC2(void);	
void setDAC1(uint16_t dac_data);
void initDmaDAC1(uint16_t * dBuff, uint16_t size);
void getData4DAC(uint16_t * dac_buff, uint8_t pb, float a1, float a2, uint16_t f1, uint16_t f2);

#define PI 					3.14159
#define DAC_BUFF_SIZE	 1000
extern volatile uint16_t g_dac_buff[DAC_BUFF_SIZE];
extern volatile uint8_t g_dac_set;
#endif 
