#include "stm32f4xx.h"
#include "delay.h"
#include "usart.h"
#include "command_parser.h"
#include "dac.h"
#include "adc.h"

#define ADC_BUFF_SIZE			(DAC_BUFF_SIZE)

void initTIM4();
void getData4DAC(uint16_t *, uint8_t, float, float, uint16_t, uint16_t);

int main(void)
{
  uint16_t utmp16;
  uint32_t utmp32, disp_timer, k = 0;
  uint16_t adc_buff0[ADC_BUFF_SIZE];
  uint16_t adc_buff1[ADC_BUFF_SIZE];
  initUSART3(USART2_BAUDRATE_921600);
  initUSART2(USART2_BAUDRATE_460800);
  enIrqUSART2();
  initDmaADC1(adc_buff0, adc_buff1, ADC_BUFF_SIZE);	// init ADC1 in double buffer DMA mode
  initDmaDAC1(g_dac_buff, DAC_BUFF_SIZE);
  initSYSTIMER();
  ///wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
  /// setup peripherals
  ///-----------------------------------------------------------------
  //LED
  RCC->AHB1ENR |= RCC_AHB1ENR_GPIODEN;  								
  GPIOD->MODER |= 0x55000000;             							
  GPIOD->OTYPER |= 0x00000000;										
  GPIOD->OSPEEDR |= 0xFF000000; 		
  GPIOD->AFR[1] |= 0x22220000;										//  
  //TIM4
  initTIM4();
  //PUSH BUTTON
  RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN;
  GPIOA->MODER &= ~0x00000003; 
    
  uint8_t led0_flag = 1;
  uint8_t led1_flag = 1;
  uint8_t led2_flag = 1;
  uint8_t led3_flag = 1;
  printUSART2("-> SYS: init completed\n");								
  ///wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww 	
  while(1)
  {
    if(g_led0_flag == 1)
    {
      if(led0_flag == 1)
      {
        if(chk4TimeoutSYSTIMER(g_led0_timer, g_led0_on_interval) == (SYSTIMER_TIMEOUT))
        {
          GPIOD->ODR &= 0xEFFF;
          led0_flag = 0;
          g_led0_timer = getSYSTIMER();
        }
      }
      else
      {
        if(chk4TimeoutSYSTIMER(g_led0_timer, g_led0_off_interval) == (SYSTIMER_TIMEOUT))
        {
          GPIOD->ODR |= 0x1000;
          led0_flag = 1;
          g_led0_timer = getSYSTIMER();
        }
      }
    }
    if(g_led1_flag == 1)
    {
      if(led1_flag == 1)
      {
        if(chk4TimeoutSYSTIMER(g_led1_timer, g_led1_on_interval) == (SYSTIMER_TIMEOUT))
        {
          GPIOD->ODR &= 0xDFFF;
          led1_flag = 0;
          g_led1_timer = getSYSTIMER();
        }
      }
      else
      {
        if(chk4TimeoutSYSTIMER(g_led1_timer, g_led1_off_interval) == (SYSTIMER_TIMEOUT))
        {
          GPIOD->ODR |= 0x2000;
          led1_flag = 1;
          g_led1_timer = getSYSTIMER();
        }
      }
    }
    if(g_led2_flag == 1)
    {
      if(led2_flag == 1)
      {
        if(chk4TimeoutSYSTIMER(g_led2_timer, g_led2_on_interval) == (SYSTIMER_TIMEOUT))
        {
          GPIOD->ODR &= 0xBFFF;
          led2_flag = 0;
          g_led2_timer = getSYSTIMER();
        }
      }
      else
      {
        if(chk4TimeoutSYSTIMER(g_led2_timer, g_led2_off_interval) == (SYSTIMER_TIMEOUT))
        {
          GPIOD->ODR |= 0x4000;
          led2_flag = 1;
          g_led2_timer = getSYSTIMER();
        }
      }
    }
    if(g_led3_flag == 1)
    {
      if(led3_flag == 1)
      {
        if(chk4TimeoutSYSTIMER(g_led3_timer, g_led3_on_interval) == (SYSTIMER_TIMEOUT))
        {
          GPIOD->ODR &= 0x7FFF;
          led3_flag = 0;
          g_led3_timer = getSYSTIMER();
        }
      }
      else
      {
        if(chk4TimeoutSYSTIMER(g_led3_timer, g_led3_off_interval) == (SYSTIMER_TIMEOUT))
        {
          GPIOD->ODR |= 0x8000;
          led3_flag = 1;
          g_led3_timer = getSYSTIMER();
        }
      }
    }
    //new functionallity
    if(g_dac_set == 1 && g_adc_set == 1)
    {
      if((DMA2_Stream0->CR & DMA_SxCR_CT) == DMA_SxCR_CT)
      {// we can read buffer 0
        for(k=0;k<((ADC_BUFF_SIZE)/2);k++)
        {
          utmp16 = adc_buff0[k];
          putcharUSART3((utmp16&0xFF00)>>8);
          putcharUSART3(utmp16&0x00FF);
        }
      }
      else
      {// we can read buffer 1
        for(k=0;k<((ADC_BUFF_SIZE)/2);k++)
        {
          utmp16 = adc_buff1[k];
          putcharUSART3((utmp16&0xFF00)>>8);
          putcharUSART3(utmp16&0x00FF);
        }
      }
    }

    chkRxBuffUSART2();
  }
}

void initTIM4()
{
  RCC->APB1ENR |= RCC_APB1ENR_TIM4EN; 							// enable TIM4 on APB1 
  TIM4->PSC = 0x0054 - 0x0001;									// set TIM4 counting prescaler 
  TIM4->ARR = 0x03E8;												// period of the PWM 1ms
  TIM4->CCR1 = 0x0000;											// duty cycle for the PWM set to 0%
  TIM4->CCR2 = 0x0000;
  TIM4->CCR3 = 0x0000;
  TIM4->CCR4 = 0x0000;
  
  TIM4->CCMR1 |= (TIM_CCMR1_OC1PE)|(TIM_CCMR1_OC1M_2)|(TIM_CCMR1_OC1M_1);
  TIM4->CCMR1 |= (TIM_CCMR1_OC2PE)|(TIM_CCMR1_OC2M_2)|(TIM_CCMR1_OC2M_1);	
  TIM4->CCMR2 |= (TIM_CCMR2_OC3PE)|(TIM_CCMR2_OC3M_2)|(TIM_CCMR2_OC3M_1);	
  TIM4->CCMR2 |= (TIM_CCMR2_OC4PE)|(TIM_CCMR2_OC4M_2)|(TIM_CCMR2_OC4M_1);					
                                    
  // set active mode high for pulse polarity
  TIM4->CCER &= ~((TIM_CCER_CC1P)|(TIM_CCER_CC2P)|(TIM_CCER_CC3P)|(TIM_CCER_CC4P));
  TIM4->CR1 |= (TIM_CR1_ARPE)|(TIM_CR1_URS);
  
  // update event, reload all config 
  TIM4->EGR |= TIM_EGR_UG;											
  // activate capture compare mode
  // start counter										
  TIM4->CR1 |= TIM_CR1_CEN;											
}


